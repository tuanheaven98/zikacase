package com.example.demo.models;

import java.util.Date;

public class ZikaCase {
    private Long id;
    private String state;
    private Integer travel_case;
    private Integer local_case;
    private Date date;

    public ZikaCase(String state, Integer travel_case, Integer local_case, Date date) {
        this.state = state;
        this.travel_case = travel_case;
        this.local_case = local_case;
        this.date = date;
    }

    public ZikaCase() {
    }


    public Integer getTravel_case() {
        return travel_case;
    }

    public void setTravel_case(Integer travel_case) {
        this.travel_case = travel_case;
    }

    public Integer getLocal_case() {
        return local_case;
    }

    public void setLocal_case(Integer local_case) {
        this.local_case = local_case;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }



    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
