package com.example.demo;

import com.example.demo.connect.DBConnect;
import com.example.demo.dtos.ChartData;
import com.example.demo.dao.CreateDAOImpl;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import java.io.File;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        CreateDAOImpl createService = new CreateDAOImpl();
        Connection conn = null;
        Statement stmt = null;
        Map<String,String[]> example = new HashMap<String, String[]>();
    try {
            conn = DBConnect.getConnection();
            createService.handleData(conn,"cdc-zika-data/");
        //Create table usa_zika_case
//        try{
//            conn = DBConnect.getConnection();
//            stmt = conn.createStatement();
//            String sqlState =  "DROP TABLE usa_zika_case IF EXISTS;" +
//                    "CREATE TABLE usa_zika_case " +
//                    "(id BIGINT not NULL AUTO_INCREMENT, " +
//                    "state VARCHAR (100) ,"+
//                    "travel_case INTEGER , "+
//                    "local_case INTEGER ," +
//                    "date DATE ," +
//                    "PRIMARY KEY ( id ))";
//            stmt.executeUpdate(sqlState);
//            System.out.println("Creating table state in given database...");
//        }catch (Exception e){
//            e.printStackTrace();
//        }


            // Execute a query select data
            System.out.println("Connected database successfully...");
                for (String state: createService.getStates()) {
                    List<ChartData> chartDatas = createService.getChartDataByState(state);
                    TimeSeries series = new TimeSeries("Number of cases for " + state);
                    TimeSeries seriesTotal = new TimeSeries("Total cases for " + state);
                    DateFormat sdf = new SimpleDateFormat("MM-yyyy");
                    Date date = null;
                    for (ChartData c: chartDatas) {
                        date = sdf.parse(c.getDate());
                        series.add(new Day(date), c.getEffectedCase());
                        seriesTotal.add(new Day(date), c.getTotalCase());
                    }
                    TimeSeriesCollection timeSeriesCollection = new TimeSeriesCollection(series);
                    timeSeriesCollection.addSeries(seriesTotal);
                    JFreeChart timechart = ChartFactory.createTimeSeriesChart(
                            "Data",
                            "Time",
                            "Cases",
                            (XYDataset) timeSeriesCollection,
                            true,
                            false,
                            false);
                    int width = 560;
                    int height = 370;
                    File timeChart = new File("./output/" + state + ".PNG");
                    ChartUtilities.saveChartAsJPEG(timeChart, timechart, width, height);
                }
            // STEP 4: Clean-up environment
//            stmt.close();
            conn.close();
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try{
                if(stmt!=null) stmt.close();
            } catch(SQLException se2) {
            } // nothing we can do
            try {
                if(conn!=null) conn.close();
            } catch(SQLException se){
                se.printStackTrace();
            } //end finally try
        } //end try
        System.out.println("Goodbye!");
    }

}
