package com.example.demo.dtos;

public class ChartData {
    String date;
    int effectedCase;
    int totalCase;
    public ChartData(IChartData chartData) {
        this.date = chartData.getDate();
        this.effectedCase = chartData.getAffectedCase();
        this.totalCase = chartData.getAffectedCase();
    }

    public ChartData(String date, int effectedCase, int totalCase) {
        this.date = date;
        this.effectedCase = effectedCase;
        this.totalCase = totalCase;
    }

    public ChartData() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getEffectedCase() {
        return effectedCase;
    }

    public void setEffectedCase(int effectedCase) {
        this.effectedCase = effectedCase;
    }

    public int getTotalCase() {
        return totalCase;
    }

    public void setTotalCase(int totalCase) {
        this.totalCase = totalCase;
    }
}
