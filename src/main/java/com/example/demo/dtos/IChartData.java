package com.example.demo.dtos;

public interface IChartData {
    String getDate();
    int getAffectedCase();
    int getTotalCase();
}
