package com.example.demo.dao;

import com.example.demo.connect.DBConnect;
import com.example.demo.dtos.ChartData;
import com.example.demo.dtos.IChartData;
import com.example.demo.models.ZikaCase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class CreateDAOImpl implements CreateDAO {
    Statement stmt = null;
    private static final String COMMA = ",";
    private static final String CSV_EXTENSION = ".csv";
    private static final String PREFIX = "cdc-state-case-counts-";
    private static final String CSV_HEADER = "state_or_territory";
    private static final String DATE_SEPARATOR = "-";
    public void createTable(Connection conn, String sql) {
        try {
            //STEP 3: Execute a query create database
            System.out.println("Creating table in given database...");
            stmt = conn.createStatement();
            stmt.executeUpdate(sql);
            System.out.println("Created table in given database...");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Date convertStringToDate(String date) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        simpleDateFormat.setLenient(false);
        return simpleDateFormat.parse(date);
    }
    private String getDateFromFileName(String fileName) {
        return fileName.split(PREFIX)[1].split(CSV_EXTENSION)[0];
    }

    public void handleData(Connection conn, String nameFolder) {
        ArrayList<ZikaCase> zikaCaseArrays = new ArrayList<ZikaCase>();
        String excelFilePath;
        File file = new File(nameFolder);
        File[] files = file.listFiles();
        for(File f: files){
                excelFilePath = f.getPath();
                try {
                    Scanner scanner = new Scanner(f);
                    BufferedReader lineReader = new BufferedReader(new FileReader(excelFilePath));
                    String lineText = null;
                    lineReader.readLine(); // skip header line
                    String state;
//                    Date date;
                    Integer travel_case;
                    Integer local_case;
                    while ((lineText = lineReader.readLine()) != null) {
                        String[] data = lineText.split(",");
                        state = data[0];
                        travel_case = Integer.parseInt(data[1]);
                        local_case = Integer.parseInt(data[2]);

//                             date = getDateFromFileName(f.getName());
//                        try {
//                            String sql = "Insert into usa_zika_case(state, travel_case,local_case, date) values(?,?,?,?)";
//                            PreparedStatement pstm = conn.prepareStatement(sql);
//                            pstm.setString(1,state);
//                            pstm.setInt(2,travel_case);
//                            pstm.setInt(3,local_case);
//                            pstm.setString(4,date);
//                            int res = pstm.executeUpdate();
//                            if(res!=0)
//                                System.out.println("Successfully import!");
//                            else
//                                System.out.println("Failed import");
//                        }
//                        catch (Exception e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
                    }
                } catch(Exception e) {
                    //Handle errors for Class.forName
                    e.printStackTrace();
                }
        }
    }

    @Override
    public List<String> getStates() {
        List<String> states = new ArrayList<>();
        try {
            Connection conn = DBConnect.getConnection();
            String sql = "select distinct uzc.state from usa_zika_case uzc order by uzc.state";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            // STEP 4: Extract data from result set
            while(rs.next()) {
                // Retrieve by column name
                String state = rs.getString("state");
                states.add(state);
            }rs.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return states;
    }


    List<IChartData> getIChartDataByState(String state){

        List<IChartData> iChartDatas = new ArrayList<>();
        try {
            Connection conn = DBConnect.getConnection();
            String sql = "select (extract(month from uzc.date)) as currMonth, (extract(year from uzc.date)) as currYear " +
                    ",sum(uzc.travel_case + uzc.local_case) as affectedCase " +
                    "FROM usa_zika_case uzc " +
                    "where uzc.STATE = ?"+
                    "group by currMonth,currYear " +
                    "order by currYear";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,state);
            ResultSet rs = pstmt.executeQuery();
            // STEP 4: Extract data from result set
            while(rs.next()) {
                // Retrieve by column name
                String currMonth = rs.getString("currMonth");
                String currYear = rs.getString("currYear");
                StringJoiner date_case = new StringJoiner("-");
                date_case.add((currMonth));
                date_case.add(currYear);
                Integer affectedCase = rs.getInt("affectedCase");
                IChartData iChartData = new IChartData() {
                    @Override
                    public String getDate() {
                        return String.valueOf(date_case);
                    }

                    @Override
                    public int getAffectedCase() {
                        return affectedCase;
                    }

                    @Override
                    public int getTotalCase() {
                        return 0;
                    }
                };
                iChartDatas.add(iChartData);
            }rs.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        return iChartDatas;
    }


    public List<ChartData> getChartDataByState(String state) {
        List<ChartData> chartDatas = new ArrayList<ChartData>();
        List<IChartData> iChartDatas = getIChartDataByState(state);
                // Display values
                int total = 0;
                for (IChartData iChartData : iChartDatas) {
//                    System.out.print("currYear: " + iChartData.getDate());
//                    System.out.print(", affectedCase: " + iChartData.getAffectedCase());
//                    System.out.print(", totalCase: " + iChartData.getTotalCase() + "\n");
                    total += iChartData.getAffectedCase();
                    ChartData chartData = new ChartData();
                    chartData.setDate(iChartData.getDate());
                    chartData.setEffectedCase(iChartData.getAffectedCase());
                    chartData.setTotalCase(total);
                    chartDatas.add(chartData);

                }
            return chartDatas;
    }
}

