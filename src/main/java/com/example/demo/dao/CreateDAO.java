package com.example.demo.dao;

import com.example.demo.models.ZikaCase;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

public interface CreateDAO {
    void createTable(Connection conn, String sql);
    Date convertStringToDate(String date) throws Exception;
    void handleData(Connection conn, String nameFolder);
    List<String> getStates();
}
