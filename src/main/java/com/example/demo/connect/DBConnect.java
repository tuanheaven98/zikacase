package com.example.demo.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {
    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:~/zikacase2";
    //  Database credentials
    static final String USER = "sa";
    static final String PASS = "";
    public static Connection getConnection() throws SQLException,
            ClassNotFoundException {
        //STEP 2: Open a connection
//            System.out.println("Connecting to database...");
        Class.forName(JDBC_DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL,USER,PASS);
        return conn;
    }
}
